from data_processing import get_allowed_characters, create_tfrecord_sequence_only
import numpy as np
import os

allowed_characters = get_allowed_characters()
aa_freq_dict = {}


def save_sequences(sequences, labels, name, motifs, seq_length, n_motifs, fraction):
    if not os.path.exists("Synthetic Data/%s/"%(name)):
        os.mkdir("Synthetic Data/%s/"%(name))

    with open ("Synthetic Data/%s/readable_pairs.csv"%(name), 'w') as outp:
        for idx, pair in enumerate(sequences):
            outp.write("%s,%s,%d\n"%(pair[0], pair[1], labels[idx]))
    with open("Synthetic Data/%s/info.txt"%(name), 'w') as outp:
        outp.write("Motif pairs:\n")
        for idx in range(0,len(motifs),2):
            outp.write("%s\t%s\n"%(motifs[idx], motifs[idx+1]))
        outp.write("Sequence Length:\t%d\n"%seq_length)
        outp.write("No. Motifs:\t%d\n" % n_motifs)
        outp.write("Label Ratio:\t%.2f\n" % fraction)

def save_pairs(pairs, save_dir):
    if not os.path.isdir(save_dir):
        os.mkdir(save_dir)
    with open("%s/training_pairs.txt"%save_dir, 'w') as outp:
        outp.write('\n'.join(["%d\t%d\t%.1f" %(s1,s2,label) for s1,s2,label in pairs[100:]]))

    with open("%s/test_pairs.txt"%save_dir, 'w') as outp:
        outp.write('\n'.join(["%d\t%d\t%.1f" %(s1,s2,label) for s1,s2,label in pairs[:100]]))

def save_sequences_to_tfrecord(sequences, motifs, save_dir, labels, minus_one_hot=False):
    dic = []
    pairs = []
    for idx, pair in enumerate(sequences):
        dic.append((idx*2, pair[0]))
        dic.append((idx*2+1, pair[1]))
        pairs.append((idx*2, idx*2+1, labels[idx]))
    dic = dict(dic)
    save_pairs(pairs, save_dir)

    # For evaluation purposes
    motif_dict = dict([("Motif%d"%i, motifs[i]) for i in range(len(motifs))])

    create_tfrecord_sequence_only(motif_dict, save_dir + '/Records/', minus=minus_one_hot)

    create_tfrecord_sequence_only(dic, save_dir + '/Records/', minus=minus_one_hot)




def generate_pair(seq_length, motif_length, motifs, fraction, var_positions=None, motifs_per_sequence=1,
                  poly_l=False, heterologs=False):
    """

    Args:
        seq_length:
        motif_length:
        motifs:
        fraction:

    Returns:
        (string,string,integer) corresponding to the first and second sequence and its respective label
                                                                                (0=non-binding, 1=binding)
    """

    seq_1 = [np.random.choice(allowed_characters) for _ in range(seq_length)]
    seq_2 = [np.random.choice(allowed_characters) for _ in range(seq_length)]

    # To test the effect of similar regions in the binding/non-binding pairs
    # Can the network learn to deal with this?
    if poly_l:
        seq_1[:10] = "L"
        seq_2[:10] = "L"

    n_motifs = len(motifs)
    # insert motif?
    prob = np.random.rand()
    if (prob < fraction):  # binding pair

        if motifs_per_sequence == 1:
            m = np.random.randint(n_motifs)
            length = len(motifs[m])
            idx = np.random.randint(0, seq_length - length)

            # Motifs are paired in order, i.e. (0,1), (2,3), ... (n-1,n)
            seq_1[idx:idx + length] = motifs[m]

            idx = np.random.randint(0, seq_length - length)
            seq_2[idx:idx + length] = motifs[m - (m % 2) + (1 - m % 2)]
        elif motifs_per_sequence == 2:

            m = np.random.randint(int(n_motifs/motifs_per_sequence))
            # Motifs are paired in order, i.e. (0,1), (2,3), ... (n-1,n)
            idx = np.random.randint(0, seq_length - motif_length)
            idx2 = np.random.randint(0, seq_length - motif_length)

            # TODO: more succint please...
            # (m%2)*-1 for reverse
            # start = np.floor(m/2)*2
            if m == 0:
                seq_1[idx:idx + motif_length] = motifs[0]
                seq_1[idx2:idx2 + motif_length] = motifs[1]
                seq_2[idx2:idx2 + motif_length] = motifs[2]
                seq_2[idx:idx + motif_length] = motifs[3]
            elif m == 1:
                seq_1[idx:idx + motif_length] = motifs[3]
                seq_1[idx2:idx2 + motif_length] = motifs[2]
                seq_2[idx2:idx2 + motif_length] = motifs[1]
                seq_2[idx:idx + motif_length] = motifs[0]
            elif m == 2:
                seq_1[idx:idx + motif_length] = motifs[4]
                seq_1[idx2:idx2 + motif_length] = motifs[5]
                seq_2[idx2:idx2 + motif_length] = motifs[6]
                seq_2[idx:idx + motif_length] = motifs[7]
            elif m == 3:
                seq_1[idx:idx + motif_length] = motifs[7]
                seq_1[idx2:idx2 + motif_length] = motifs[6]
                seq_2[idx2:idx2 + motif_length] = motifs[5]
                seq_2[idx:idx + motif_length] = motifs[4]
    else:  # No binding
        if heterologs == True:
            idx = np.random.randint(0, seq_length - motif_length)
            m = np.random.choice(motifs)
            seq_1[idx:idx + motif_length] = m

            randint = np.random.randint(0,2)
            if randint == 0:
                m = np.random.choice(motifs) # comment for autologs
                idx  = np.random.randint(0, seq_length - motif_length)
                seq_2[idx:idx + motif_length] = m

        # else only one of the sequences contains a motif
        else:
            # Do we still insert a motif in one of the sequences?
            if motifs_per_sequence == 1:
                # Which sequence?
                if (prob > (1 - fraction) + 0.5 * fraction):
                    idx = np.random.randint(0, seq_length - motif_length)
                    m = np.random.choice(motifs)
                    seq_1[idx:idx + motif_length] = m
                elif (prob > (1 - fraction)):
                    idx = np.random.randint(0, seq_length - motif_length)
                    m = np.random.choice(motifs)
                    seq_2[idx:idx + motif_length] = m
            elif motifs_per_sequence == 2:
                # two random motifs when multiple motifs are required for binding
                idx = np.random.randint(0, seq_length - motif_length)
                m = np.random.choice(motifs)
                seq_1[idx:idx + motif_length] = m

                idx = np.random.randint(0, seq_length - motif_length)
                m = np.random.choice(motifs)
                seq_2[idx:idx + motif_length] = m

    seq_1 = ''.join(seq_1)
    seq_2 = ''.join(seq_2)

    # Determine whether two complementary motifs are present
    present_1 = [int(motif in seq_1) for motif in motifs]
    present_2 = [int(motif in seq_2) for motif in motifs]

    current_label = 0
    for i in range(len(motifs)):
        neighbour = i - (i % 2) + (1 - i % 2) # motifs are paired as (0,1), (2,3), (4,5) ...
        if motifs_per_sequence == 1:
            if (present_1[i] + present_2[neighbour]) == 2 or (present_2[i] + present_1[neighbour]) == 2:
                current_label = 1.0
                break
        elif motifs_per_sequence == 2:
            for x in range(int(n_motifs/4)):
                if ((present_1[4*x+0] + present_1[4*x+1] + present_2[4*x+2] + present_2[4*x+3]) == 4 or (present_1[4*x+3] + present_1[4*x+2] + present_2[4*x+1] + present_2[4*x+0]) == 4):
                    current_label = 1.0
                    break

    if isinstance(var_positions, list):
        pass

    return(seq_1, seq_2, current_label)


def generate_sequences_and_labels(n_sequences, seq_length, name, n_motifs=4, motif_length=5,
                           custom_motifs=None, fraction=0.5, minus_one_hot=False, required_pairs=1, heterologs=False):
    """

    Args:
        n_sequences:
        seq_length:
        name:
        n_motifs:
        motif_length:
        custom_motifs:
        fraction:

    Returns:

    """

    if (n_motifs % 2 == 1):
        print("Number of motifs was an odd number, aborting.")
        exit()

    if type(custom_motifs) == type(None):
        print("No custom motifs provided, generating new motifs.")
        motifs = []
        for i in range(n_motifs):
            motif = [np.random.choice(allowed_characters) for _ in range(motif_length)]
            motif = ''.join(motif)
            motifs.append(motif)
    else:
        print("Using user-provided motifs to generate sequences.")
        motifs = custom_motifs
        n_motifs = len(motifs)
        motif_length = len(motifs[0])

    sequences = []
    labels = []
    print("Generating sequence pairs...")
    for i in range(n_sequences):
        seq_1, seq_2, label = generate_pair(seq_length, motif_length, motifs, fraction,
                                            motifs_per_sequence=required_pairs, heterologs=heterologs)
        labels.append(label)
        sequences.append([seq_1, seq_2])
    # Human readable
    save_sequences(sequences, labels, name, motifs, seq_length, n_motifs, fraction)
    # Binary
    save_sequences_to_tfrecord(sequences, motifs, "Synthetic Data/%s"%name, labels,minus_one_hot=minus_one_hot)
    return(sequences, labels)

def generate_random_sequences(n_sequences, save_dir, length=600):
    """
    Creates two folders in save_dir: Fasta and Records.
    The Fasta folder is populated with .txt files containing the raw (random) amino acid sequences.
    The Records folder is populated with TFRecord files corresponding to the .txt files in the Fasta folder.

    :param n_sequences:
    :param save_dir:
    :param length:
    :return:
    """
    seq_dict = {}

    if not os.path.exists("%s/Fasta"%save_dir):
        os.mkdir("%s/Fasta"%save_dir)
    if not os.path.exists("%s/Records" % save_dir):
        os.mkdir("%s/Records" % save_dir)

    for i in range(n_sequences):
        seq = [np.random.choice(allowed_characters) for _ in range(length)]

        with open("%s/Fasta/%d.txt"%(save_dir,i),'w') as outp:
            outp.write(''.join(seq))

        seq_dict["random_%d"%i] = seq

    create_tfrecord_sequence_only(seq_dict, "%s/Records"%save_dir)

def main():
    n_motifs = 14
    motifs = None
    # Heterolog sequences appear to be harder to model, if true then the negative pairs will be of the format (A,Z)
    # Where A and Z are sequences containing non-complementary motifs
    heterologs = False

    use_custom_motifs = True


    if use_custom_motifs: #Using known motifs
        motifs = """MSYVI	MHEYV
                    LEARP	PCDKG
                    VHHSG	EHHWT
                    CGFWS	VFNLF
                    LSLVH	HLDQK
                    VDNLP	QPRKG
                    LQRVY	SKRME"""


        motifs = motifs.split()
        n_motifs = len(motifs)

    # What percentage of the dataset consists of binding sequences
    frac = 0.5

    length = 500

    add = ''
    if heterologs:
        add = '_heterologs'

    sequences, labels = generate_sequences_and_labels(2000, length,
                                                      name="Synthetic_%d_length%d_frac%.2f%s"%(n_motifs,
                                                      length, frac, add), fraction=frac, n_motifs = n_motifs,
                                                      minus_one_hot=False, custom_motifs=motifs,
                                                      required_pairs=1, heterologs=heterologs)



if __name__ == "__main__":
    main()
