from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from sklearn.metrics import roc_curve, auc, precision_score, accuracy_score, confusion_matrix

import tensorflow as tf
import numpy as np
import gc
import os

from data_processing import *

allowed_characters = get_allowed_characters()

class PPI_CNN(object):

    def one_of_k(self, val, k):
        result = np.zeros(k)
        result[val] = 1.0
        return result

    def load_pairs(self, fname, data_dir):
        """
        Indexes the sample pairs and their respective labels.
        :param fname: string, name of the file containing the pairs. Expected format 'TFRecord_Name1\tTFRecord_Name2\tLabel\n'
        :param data_dir: string, directory containing all TFRecords
        :return:
            list of lists, where the first index corresponds to all the 'TFRecord_Name1' entries. Similarly, the second
            corresponds to all 'TFRecord_Name2' entries.
        """
        pair = [[], []]
        self.combined_pairs = []
        with open(fname, 'r') as inp:
            for string in inp.read().split('\n')[:-1]:
                self.combined_pairs.append(string)
                sP = string.split('\t')

                pair[0].append("%s/%s" %(data_dir, sP[0]))
                pair[1].append("%s/%s" %(data_dir, sP[1]))
                if self.n_prediction_nodes == 1:
                    self.labels.append(float(sP[2]))
                else:
                    print("Using %d prediction nodes")
                    self.labels.append(self.one_of_k(int(sP[2]), self.n_prediction_nodes))

        return pair

    def __init__(self, data_dir, batch_size, seq_length, kern_size, n_fil, pair_file, false_start_rate,
                 init="uniform", io_threads=15, restore_model="none", prediction_nodes=1, bound=3.0):

        self.n_prediction_nodes = prediction_nodes
        tf.reset_default_graph()

        if restore_model != "none":
            print("Restoring trained model...")
            self.sess = tf.Session()
            saver = tf.train.import_meta_graph(restore_model + 'model.meta')
            saver.restore(self.sess, tf.train.latest_checkpoint(restore_model))
            print("Model restored.")
            return

        print("Indexing samples...")

        self.init = init
        self.labels = []
        self.pair_list = self.load_pairs(pair_file, data_dir)
        self.data_threads = io_threads

        print("Done indexing %d interactions." %(len(self.pair_list[0])))
        self.batch_size = batch_size
        self.seq_length = seq_length
        self.kern_size = kern_size  # Kernel size, both width and height
        self.n_fil = n_fil
        self.bound = bound # Maximum possible filter output
        self.false_start_rate = false_start_rate

    def __del__(self):
        #tf.reset_default_graph()
        self.sess.close()
        gc.collect()

    def _create_paired_multiplication_layer(self, combined_pool):
        """
        Creates a paired multiplication layer, where each filter output only has one other
        filter output with which it is multiplied.
        Args:
            combined_pool:  output of the the max pool layers of both CNNs

        Returns:
            Tensor representing the output of the filters after multiplication.

        """
        # For all complimentary filter pairs (A,B), we multiply the output of A by B.
        with tf.variable_scope("Pairing"):
            # Although looping twice is more inefficient, the tensorflow graph will be better organized
            with tf.variable_scope("Multiplication"):
                multi = []
                for i in range(self.n_fil):
                    # j is the index of the complementary filter
                    j = (i+self.n_fil)-(i%2)+(1-i%2)
                    multi_ad = tf.multiply(combined_pool[:, i], combined_pool[:, j],name="MultipliedPair%d_%d" % (i, j))

                    multi.append(multi_ad)

            # A matrix is created containing the output of filter A for all complementary pairs (A,B)
            # This is done to deal with the event of filter B not being turned 'on' yet (due to false start)
            with tf.variable_scope("MaxReduce"):
                maxi = []
                for i in range(self.n_fil):
                    # j is the index of the complementary filter
                    j = (i + self.n_fil) - (i % 2) + (1 - i % 2)
                    if i % 2 == 0:
                        maxi_ad = combined_pool[:, i]
                    else:
                        maxi_ad = combined_pool[:, j]
                    maxi.append(maxi_ad)

            multi = tf.stack(multi)

            if self.false_start_rate > float(0.0):
                with tf.variable_scope("FalseStartConditional"):
                    maxi = tf.stack(maxi)**2
                    # Create a vector with values corresponding to the iteration number at which the filter found at
                    # that index should become trainable.
                    sequential_start_masks = tf.constant([int(k*((self.false_start_rate*self.max_iterations)/(
                        self.n_fil-1))) for k in range(self.n_fil)])

                    # If false start is used then we initially ignore the activation of the complimentary filter
                    out = tf.where(self.iteration > sequential_start_masks, multi, maxi)
            else:
                out = multi

            return(out)

    def _orientation_correction(self, uncorrected_pairs):
        """
        Chooses the maximum value between filter pair activation (A,B) and (B,A)

        :param uncorrected_pairs: Tensor of size [batch_size, self.n_fil]
        :return:
            output, Tensor of size [batch_size, 0.5*self.n_fil]
        """
        with tf.variable_scope("OrientationCorrection"):
            motifs = []
            # Here the activation of motif pairs (A,B) and (B,A) are merged into a single value
            with tf.variable_scope("Corrections"):
                for pair_idx in range(0, self.n_fil, 2):
                    # Pick the orientation that has the highest activation
                    act = tf.maximum(uncorrected_pairs[pair_idx], uncorrected_pairs[pair_idx+1],
                                     name="Correction%d_%d"%(pair_idx, pair_idx+1))
                    motifs.append(act)
            output = tf.stack(motifs, axis=1)
        return(output)

    def _initialize_filter(self):
        """
        Initializes the filters used in the convolution operation. How a filter is initialized depends on the value
        of self.init.
        :return: Tensor, filters that are not yet normalized.
        """
        if self.init == "uniform":
            print("Using uniform filter initialization.")
            self.raw_filter_weights = tf.Variable(tf.fill([self.kern_size, len(allowed_characters), self.n_fil], 0.05),
                                          name="raw_filters")
            return self.raw_filter_weights

        elif self.init == "normal":
            print("Using random normal filter initialization.")
            random_matrix = tf.random_normal([self.kern_size, len(allowed_characters), self.n_fil], stddev=0.5)
            self.raw_filter_weights = tf.Variable(random_matrix, name="raw_filters")
            return self.raw_filter_weights

        elif self.init == "seminormal":
            print("Using semi random normal filter initialization.")
            random_matrix = tf.random_normal([self.kern_size, len(allowed_characters), self.n_fil], mean=1.0,
                                             stddev=0.1)
            uni_matrix = tf.fill([self.kern_size, len(allowed_characters), self.n_fil], 1.0)

            self.raw_filter = [random_matrix[:, :, i] if i % 2 == 0 else uni_matrix[:, :, i] for i in range(
                self.n_fil)]
            self.raw_filter_weights = tf.Variable(tf.reshape(self.raw_filter, [self.kern_size, len(allowed_characters),
                                                                       self.n_fil]), name="raw_filters")
            return self.raw_filter_weights
        elif self.init == "actual":
            actual_motifs = np.load("actual_motifs.npy")

            return(tf.Variable(actual_motifs, name="filters", dtype=tf.float32))

        else:
            print("No valid initialization chosen! Choose from {'uniform', 'normal'. 'seminormal'}")
            exit()

    def _normalize_filter(self):
        """
        Applies activation/normalization on the filter weights, as specified in self.filter_activation.

        :return: Tensor, filters after applying normalization.
        """
        with tf.variable_scope("Normalization"):
            if self.filter_activation == "softmax":
                print("Using softmax filters.")
                soft_filter = tf.nn.softmax(self.raw_filter, dim=1) * self.bound / self.kern_size
                return( soft_filter )

            elif self.filter_activation == "normalized":
                print("Using normalized filters.")
                self.raw_filter = tf.exp(self.raw_filter)
                filter_norm = tf.reduce_sum(self.raw_filter, axis=[0, 1], keep_dims=True)
                normalized_filter = (self.raw_filter / filter_norm) * self.bound
                return normalized_filter

            elif self.filter_activation == "relu":
                print("Using ReLU filters")
                self.expected_pair_output = 0.05
                return(tf.nn.relu(self.raw_filter))

            elif self.filter_activation == "sigmoid":
                print("Using Sigmoid filters.")
                self.expected_pair_output = self.kern_size
                return(tf.nn.sigmoid(self.raw_filter))

            else:
                print("Warning: No valid filter activation selected, defaulting to using no filter activation.")
                return self.raw_filter

    def _apply_false_start(self):
        """
        Applies false start; initially silences all but the first filter, then stops silencing the next filter
        after a given number of iterations until all of them are trainable.
        :return:
        """
        with tf.variable_scope("SequentialFilterStart"):
            # Creates a matrix of masks, where the first entry has nonzero axes = [0]
            # For the second entry [0,1], the third entry [0,1,2], etc. '
            sequential_start_masks = tf.constant([[1.0 if k < i + 1 else 0.0 for k in range(self.n_fil)]
                                                  for i in range(self.n_fil)])

            # after which iteration all filters should be active, divided by the amount of filters
            denom = tf.cast((self.false_start_rate * self.max_iterations) / self.n_fil, dtype=tf.int32)

            # choose the approriate mask from the mask matrix, given the current iteration
            fraction = tf.minimum(tf.cast(self.iteration / denom, dtype=tf.int32, name="ElapsedEpochs"), self.n_fil - 1)
            sq_mask = sequential_start_masks[fraction]
            return(self.filter * sq_mask)

    def _apply_filter_dropout(self, dropout_rate):
        """
        Sets filter weights to 0.0 with a given probability. if adaptive dropout is used this probability decreases
        the longer training persists.
        :param dropout_rate: float between 0.0 and 1.0, indicates the (initial) dropout rate.
        :return:
        """
        with tf.variable_scope("FilterDropout"):
            if self.adaptive_dropout:
                print("Using adaptive dropout.")
                keep_prob = tf.minimum((1 - dropout_rate) + (tf.cast(self.iteration, dtype=tf.float32) /
                                                             tf.cast(self.max_iterations, dtype=tf.float32)), 1.0)
            else:
                print("Using static dropout.")
                keep_prob = 1 - dropout_rate
            mask = tf.constant([1.0 for _ in range(self.n_fil)])
            mask = tf.nn.dropout(mask, keep_prob=keep_prob, name="DropoutMask")
            return(self.filter * mask)

    def _apply_edge_masking(self):
        """
        Applies edge masking; the first and last two positions of all filters are set to 0.
        The filters are assumed to be stored in self.filter.

        :return: Tensor, the filter after masking the edges.
        """
        with tf.variable_scope("EdgeMasking"):
            print("Using edge masking.")
            edge_mask = tf.constant([0.0 if x < 2 or x > (self.kern_size - 3) else 1.0
                                     for x in range(self.kern_size)], name="EdgeMask")

            edge_mask = tf.reshape(edge_mask, shape=[self.kern_size, 1, 1])

            masked_filter = tf.cond(tf.greater(self.iteration, int((self.false_start_rate + 0.05) *
                                  self.max_iterations)),
                                  true_fn=lambda: self.filter,
                                  false_fn=lambda: self.filter * edge_mask)

            return(masked_filter)

    def _create_convolution_layer(self, sample_seq1, sample_seq2, dropout_rate=0.0, evaluating=False):
        """
        Creates the convolution operation in the Tensorflow graph. In this operation two input sequences are scanned
        with a set of filters and, for each filter, only the maximum cross-correlation between the filter and the
        input sequence is scanned.

        :param sample_seq1: Tensor,
        :param sample_seq2: Tensor,
        :param dropout_rate:    float, value between 0.0 and 1.0 indicating what fraction of the neurons are subject
                                to dropout.
        :param evaluating:  bool, indicates whether or not the Tensorflow graph is constructed for evaluation purposes.
        :return:
        """
        with tf.variable_scope("Conv"):
            # Filter without normalization
            self.raw_filter = self._initialize_filter()
            # Filter after normalization
            self.filter = self._normalize_filter()

            # Don't use dropout when evaluating network performance
            if not evaluating:
                if self.false_start_rate > 0.0:
                    self.filter = self._apply_false_start()

                if dropout_rate > 0.0:
                    self.filter = self._apply_filter_dropout(dropout_rate)

            # Apply edge silencing
            if self.silence_edges:
                self.filter = self._apply_edge_masking()

            padding = "SAME"
            # First sequence in the pair
            with tf.variable_scope("FirstSeq"):
                self.feature_map_1 = tf.nn.conv1d(sample_seq1, filters=self.filter, stride=1, padding=padding,
                                              name="conv",use_cudnn_on_gpu=True)
                first_pool = tf.reduce_max(self.feature_map_1, axis=1)

            # Seqond sequence in the pair
            with tf.variable_scope("SecondSeq"):
                self.feature_map_2 = tf.nn.conv1d(sample_seq2, filters=self.filter, stride=1, padding=padding,
                                               name="conv", use_cudnn_on_gpu=True)
                second_pool = tf.reduce_max(self.feature_map_2, axis=1)

            combined_pool = tf.concat([first_pool, second_pool], axis=-1, name="combine_pools")

            return (combined_pool)

    def _divide_attention(self, multi):
        """
        Scales the output of filter pairs relative to all other filter pairs. The idea is to reduce the effect of
        backpropgation on lowly-activated filters and introduces sparsity in filter pair activation.

        :param multi:   Tensor, [batch_size, int(0.5*n_fils)]
        :return:
            Tensor, [batch_size, int(0.5*n_fils)]
        """
        with tf.variable_scope("DivideAttention"):
            print("Using attention on the paired filter outputs.")
            mask = tf.nn.softmax(multi * 1000.0, dim=-1, name="AttentionMask")
            output = multi * mask
            return output

    def _create_training_nodes(self, initial_lr):
        """
        Set the cost function and optimizer.

        :param initial_lr: float, indicates the (initial) learning rate.
        :return:
        """
        with tf.variable_scope("Training"):
            if self.loss_func == "cross-entropy":
                print("Using sigmoid cross-entropy loss.")
                if self.n_prediction_nodes == 1:
                    self.cost = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=self.batch_labels,
                                                                            logits=self.prediction))
                else:
                    print("Using softmax cross-entropy loss for multi-label classifcation.")
                    self.cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=self.batch_labels,
                                                                                logits=self.prediction))
            elif self.loss_func == "l2":
                print("Using l2 loss.")
                self.cost = tf.nn.l2_loss(self.prediction-self.batch_labels, name="cost")
            else:
                print("No valid loss function chosen. Select 'cross-entropy' or 'l2'.")
                exit()

            # Activation sparsity regularizer
            # Increases with the number of iterations passed
            sparsity_coeff = 0.07 * tf.nn.sigmoid(((100.0*tf.cast(self.iteration,
                             dtype=tf.float64))/self.max_iterations) - 50)

            self.cost = self.cost + sparsity_coeff * tf.cast(self.sparsity_term, dtype=tf.float64)

            tf.summary.scalar("summaries/Cost", self.cost)
            #initial_lr = tf.train.exponential_decay(initial_lr, global_step=self.iteration, decay_steps=1,
            #                                        decay_rate=0.99995)
            self.initial_lr = initial_lr
            self.Adam = tf.train.AdamOptimizer(learning_rate=initial_lr)
            self.optimizer = self.Adam.minimize(self.cost)

            # Increment the iteration number
            self.increment_iteration = self.iteration.assign_add(1)
            self.merged_summaries = tf.summary.merge_all()

    def create_logic_prediction_layer(self, and_gates):
        """
        This is where the orientation-corrected filter pairs are connected to the prediction node.
        If the weights are trainable, then the network can learn how important each filter pair is, and it can be
        learned whether a filter pair stimulates or inhibits binding. When it is
        untrainable then
        :param and_gates:
        :return:
        """
        with tf.variable_scope("Prediction"):
            n = int(0.5*self.n_fil)
            and_kern_init = tf.ones([n, 1], dtype=tf.float32)
            #and_kern_init = tf.ones([n, 1], dtype=tf.float32) * 0.2

            and_sum = tf.layers.dense(and_gates, 1, use_bias=False, activation=None, trainable=True,
                                      kernel_initializer=lambda x,  dtype, partition_info: and_kern_init)

            # 1/400 is the expected value of the filter pair output at the first iteration
            bias = - tf.exp(tf.Variable(tf.log(self.n_fil/400.0)))
            pred = and_sum + bias
            self.prediction = tf.cast(pred, tf.float64, name="Prediction")

    def init_graph(self, initial_lr, dropout, evaluation=False):
        """

        Args:
            batch_size: int, specify the number of sequence pairs to use for each run
            initial_lr: float, initial learning rate (for adaptive gradient
                        optimizers), or the static learning rate, e.g. for gradient
                        descent.

        Returns:
            None
        """

        # Iteration number
        self.iteration = tf.Variable(0, dtype=tf.int32, name='iteration_no', trainable=False)

        # Get shuffled padded batches from the dataset
        sample_seq1, sample_seq2, self.batch_labels, self.iter_initializer = sequence_processing(
                batch_size=self.batch_size, pair_list=self.pair_list, labels=self.labels,
                threads=self.data_threads, shuffle=not evaluation, n_prediction_nodes=self.n_prediction_nodes)

        # The batches for the sequences in a pair
        seq1 = tf.placeholder_with_default(sample_seq1, [None, None, 20], "seq1")
        seq2 = tf.placeholder_with_default(sample_seq2, [None, None, 20], "seq2")

        # Output of the convolutional layer containing 2*n_fil values per pair
        combined_pool = self._create_convolution_layer(seq1, seq2, dropout_rate=dropout)

        # The maximum value resulting from convolution with a filter (on a one-hot encoded sequence) is the sum of
        # the maximum values on the columns.
        self.maximum_filter_activation = tf.reduce_sum(tf.reduce_max(self.filter, axis=1), axis=0,
                                                                  keep_dims=True)

        # For each filter there is a trainable variable expressing the rectification threshold as a fraction of the
        # maximum possible output of that filter.
        fraction = 0.5 + 0.5 * tf.nn.sigmoid(tf.Variable(tf.ones([1, self.n_fil]) * 1.0, dtype=tf.float32))
        # Give the network some leeway
        rect_bias = fraction * self.maximum_filter_activation
        cat_bias = tf.concat([rect_bias, rect_bias], axis=-1)

        # This is where we get information on where, in the sequence, the convolution operation finds matches with
        # the filter that are above the learnt threshold. Note that this is not used for training, but only for
        # evaluation purposes (i.e. for interpretation purposes after the model is trained).
        rect_feature_map_1 = self.feature_map_1 - rect_bias
        rect_feature_map_1 = tf.where(rect_feature_map_1 > 0.0, rect_feature_map_1 + rect_bias, tf.zeros_like(rect_feature_map_1))

        self.nonzero_idx_fm1 = tf.where(rect_feature_map_1 > 0.0)
        self.nonzero_vals_fm1 = tf.gather_nd(self.feature_map_1, self.nonzero_idx_fm1)

        rect_feature_map_2 = self.feature_map_2 - rect_bias
        rect_feature_map_2 = tf.where(rect_feature_map_2 > 0.0, rect_feature_map_2 + rect_bias, tf.zeros_like(rect_feature_map_2))
        self.nonzero_idx_fm2 = tf.where(rect_feature_map_2 > 0.0)
        self.nonzero_vals_fm2 = tf.gather_nd(self.feature_map_2, self.nonzero_idx_fm2)


        # Back to the operations relevant for training
        # Rectify the filter output
        combined_pool = tf.nn.relu(combined_pool - cat_bias)
        # Pretend rectification did not happen for non-zero outputs, but ignore the gradients for this operation.
        # Otherwise the gradient of the rectification bias with respect to the cost function would always be 0.
        combined_pool = tf.where(combined_pool > 0.0, combined_pool + tf.stop_gradient(cat_bias), combined_pool)

        # public for printing purposes during training.
        self.combined_pool = combined_pool

        # Multiply the filter outputs together in a pairwise manner.
        and_gate = self._create_paired_multiplication_layer(combined_pool)

        # Only keep max((A,B), (B,A)) for complementary filters A and B.
        corrected_and_gate = self._orientation_correction(and_gate)

        if self.attention_mask == True:
            #corrected_and_gate = self._divide_attention(corrected_and_gate)
            # Adding a sparsity term to the cost function may work better than attention
            #TODO: set up experiment to compare sparsity vs attention
            self.sparsity_term = tf.reduce_mean(tf.reduce_sum(corrected_and_gate, axis=-1) - tf.reduce_max(
                corrected_and_gate, axis=-1))
            #self.sparsity_term = tf.reduce_mean(tf.reduce_sum(corrected_and_gate, axis=-1))







        corrected_and_gate = tf.identity(corrected_and_gate, name="AndGates")

        self.is_evaluating = tf.Variable(False, trainable=False, name="Evaluating", dtype=tf.bool)

        #corrected_and_gate = tf.cond(self.is_evaluating, true_fn=lambda:corrected_and_gate,
        #                                                    false_fn=lambda: tf.nn.dropout(corrected_and_gate,0.5))

        self.corrected_and_gate = corrected_and_gate
        self.create_logic_prediction_layer(corrected_and_gate)

        if not evaluation:
            self._create_training_nodes(initial_lr=initial_lr)

        #self.filter_grads = tf.gradients(self.cost, self.raw_filter_weights)

    def run_iteration(self):
        """
        Function called by every thread during training. Performs one forward and backward pass through the NN.
        Additionally, the batch cost is printed every 100 iterations.

        :param reset: Bool, indicating whether or not to use random filter reset.
        :return: None
        """
        self.sess.run(self.increment_iteration)
        iteration = self.sess.run(self.iteration)

        # Join threads if the max number of iterations has been reached
        if iteration >= self.max_iterations:
            self.coord.request_stop()
        else:
            #filter_grads = self.Adam.compute_gradients(self.cost, raw_filters)
            #_, grads, labels = self.sess.run([self.optimizer, self.filter_grads, self.batch_labels])
            #self.track_gradients.append(grads)
            #self.track_labels.append(labels)

            # Run training operation
            self.sess.run(self.optimizer)

            # Every 100 iterations show the current batch cost
            if iteration % 100 == 0 and iteration < self.max_iterations:
                cost, summary, gates, pred = self.sess.run([self.cost, self.merged_summaries,
                                                             self.corrected_and_gate, self.prediction])
                print("%d:Batch cost: %f" % (iteration, cost))
                print(gates[0])
                print(pred[0])

                # If we are tracking how the filter changes over time
                if self.track_filter_changes:
                    fil = self.sess.run(self.filter)
                    # add the filter weights to the filter list
                    self.fil_changes.append(fil)

                # Tensorboard summary
                self.writer.add_summary(summary, iteration)
                print("Rounded:", round(cost, 2), "Target:", round(self.early_stop, 2))

                # If we want to stop training when a certain batch cost is reached
                if round(self.early_stop, 2) != 0.00 and round(cost, 2) <= round(self.early_stop, 2):
                    print("Early stop threshold reached")
                    self.coord.request_stop()

    def train(self, n_iters=3000, initial_lr=1e-4, save_filters="changing_filter", restore_model=None, \
              save_to="TrainedModels/model", dropout=0.0, adaptive_dropout=False, filter_activation="softmax",
              training_threads=15, track_filter_changes=False, random_seed=None, silence_edges=True,
              early_stop=0.00, attention_mask=False, loss_func="cross-entropy"):

        self.track_gradients = []
        self.track_labels = []

        # Did we set a random seed?
        if type(random_seed) != type(None):
            tf.set_random_seed(random_seed)

        if not os.path.isdir(save_to):
            os.mkdir(save_to)

        # Set hyperparameters / architectural details
        self.loss_func = loss_func
        self.attention_mask = attention_mask
        self.early_stop = early_stop
        self.fil_changes = []
        self.max_iterations = n_iters
        self.adaptive_dropout = adaptive_dropout
        self.filter_activation = filter_activation
        self.track_filter_changes = track_filter_changes
        self.silence_edges = silence_edges

        # Clean up any residual TensorFlow variables
        tf.reset_default_graph()
        restore_sess = tf.Session()

        # If the user wants to continue training from a previously saved model
        if restore_model is not None:
            # Restore the old graph
            saver = tf.train.import_meta_graph(restore_model + 'model.meta')
            saver.restore(restore_sess, tf.train.latest_checkpoint(restore_model))

            # Get the variable values
            weights = restore_sess.run(tf.trainable_variables())

            # Remove this session
            tf.reset_default_graph()
            restore_sess.close()

            # Restored model does not need initial help with learning of the filters
            print("Sequential filter start and edge silencing are turned off because a previous model is restored!")
            self.false_start_rate = 0.0
            self.silence_edges = False

        # Create new TensorFlow session
        self.sess = tf.Session()

        # Create new network
        self.init_graph(initial_lr=initial_lr, dropout=dropout)
        self.sess.run(tf.global_variables_initializer())

        print("Filters will be saved to '%s'" %save_to)
        with open("%s/description.txt" %(save_to),'w') as outp:
            outp.write("Loss function:\t%s\n"
                       "Attention Mask:\t%s\n"
                       "Iterations:\t%d\n"
                       "Silence Edges:\t%s\n"
                       "Initial LR:\t%f\n"
                       "Initialization:\t%s\n"
                       "Filter Activation:\t%s\n"
                       %(self.loss_func, str(self.attention_mask), self.max_iterations, str(self.silence_edges),
                         initial_lr, self.init, self.filter_activation))

        # TensorBoard storage
        graph_dir = "./graphs/"
        if not os.path.exists(graph_dir):
            os.mkdir(graph_dir)
        runs = [int(x) for x in os.listdir(graph_dir) if os.path.isdir("%s/%s"%(graph_dir,x))]
        if len(runs) == 0:
            runs = [0]
        num = max(runs) + 1

        # Summary writer so TensorBoard can be used
        self.writer = tf.summary.FileWriter('./graphs/%d'%(num), self.sess.graph)
        print("Session started.")

        # Initialize the dataset iterator
        self.sess.run(self.iter_initializer)
        print("Variables initialized.")

        # Multithreading stuff
        self.coord = tf.train.Coordinator()
        self.threads = tf.train.start_queue_runners(sess=self.sess,
                                                    coord=self.coord)

        self.sess.run(self.is_evaluating.assign(False))

        if restore_model is not None:
            print("Initializing trainable variables with values from the restored model...")
            # Assigns the weights from the previously restored model to the new model.
            # The reason we can't directly use the restored model is that the used dataset is hardcoded into the
            # restored graph. This means we'd be training with the same samples again.

            for idx, w in enumerate(tf.trainable_variables()):
                self.sess.run(tf.assign(w, weights[idx]))
            print("Done.")

        print("Training...")

        # Multithreading
        threads = [tf.train.LooperThread(self.coord, None, self.run_iteration)
                   for i in range(training_threads)]

        for t in threads: t.start()

        # Join threads
        self.coord.join()

        # Save the trained model so that it can be loaded for further training / evaluation
        model_saver = tf.train.Saver()
        print("Saving model to %s" %(save_to))
        model_saver.save(self.sess, save_to + '/model')

        self.writer.close()

        # Create folder for the filters
        if save_filters[-1] == "/":
            save_filters = save_filters[:-1]
        if not os.path.isdir(save_filters):
            os.mkdir(save_filters)

        # Save weights
        trainable_weights = self.sess.run(tf.trainable_variables())

        for n, weight in enumerate(trainable_weights):
            np.save("%s/weight_%d" % (save_to, n), weight)

        self.evaluated_filters = self.sess.run(self.filter)
        np.save("%s/filters" % save_filters, self.evaluated_filters)

        # Save filter changes throughout time
        if track_filter_changes:
            # Save filter changes
            fils = np.stack(self.fil_changes)
            np.save("%s/filter_changes"%save_filters, fils)

        #np.save("%s/filter_gradients"%save_filters, np.stack(self.track_gradients))
        #np.save("%s/tracked_labels" % save_filters, np.stack(self.track_labels))

    def _evaluation_metrics(self, real, predictions):
        roc = roc_curve(real, predictions)
        accuracy = accuracy_score(real, np.round(predictions))
        precision = precision_score(real, np.round(predictions))
        tn, fp, fn, tp = confusion_matrix(real, np.round(predictions)).ravel()
        specificity = tn / (tn + fp)

        return(roc, accuracy, precision, specificity)

    def evaluate(self, evaluation_pair_list, evaluation_label_list, batch_size=50):
        """
        Performs a forward pass through the trained network with a set of evaluation sequences. It will return the
        average difference between the predicted and actual labels for those samples.
        :param evaluation_pair_list: List of lists, where the first entry is a list of paths to the the first
        sequences of a pair. Similarly, the second entry is a list of paths with the complementary sequences in a
        pair, such that evaluation_pair_list[0][idx] and evaluation_pair_list[1][idx] belong together.
        :param evaluation_label_list: List of labels corresponding to the pairs provided in evaluation_pair_list
        :return:
            roc,
            area,
        """

        labels = [float(x) for x in evaluation_label_list]

        sample_seq1, sample_seq2, batch_labels, iter_initializer = sequence_processing(
            batch_size=batch_size, pair_list=evaluation_pair_list, labels=evaluation_label_list,
            threads=1, shuffle=False)

        with tf.Session() as sample_sess:
            # Seperate session to deal with Dataset object
            sample_sess.run(tf.global_variables_initializer())
            sample_sess.run(iter_initializer)
            self.coord = tf.train.Coordinator
            tf.train.start_queue_runners(sample_sess, self.coord)

            with self.sess as sess:
                # Assign the max iteration, this is necessary to circumvent edge silencing etc. which are based upon the
                # iteration number
                self.iteration = tf.global_variables()[0]
                sess.run(self.iteration.assign(100000))
                sess.run(self.is_evaluating.assign(True))

                print("Evaluating..")
                predictions = []
                activities = []

                # Here we store the information on where, in each sequence, the filters find matches.
                position_info = []

                for batches in range(int(len(evaluation_pair_list[0])/batch_size) + 1):
                    print("Batch ", batches)
                    seq1, seq2, labs = sample_sess.run([sample_seq1, sample_seq2, batch_labels])
                    prediction = sess.graph.get_tensor_by_name("Prediction/Prediction:0")
                    pair_activity = sess.graph.get_tensor_by_name("AndGates:0")

                    # Should a sigmoid activation function be applied to the prediction
                    activity, pred, fm1_idx, fm1_vals, fm2_idx, fm2_vals = sess.run([pair_activity, tf.nn.sigmoid(
                                prediction),self.nonzero_idx_fm1, self.nonzero_vals_fm1, self.nonzero_idx_fm2,
                                                                                     self.nonzero_vals_fm2],
                                    feed_dict={ "seq1:0": seq1,
                                                "seq2:0": seq2})

                    # Set the sample number correctly
                    fm1_idx[:,0] += batches * batch_size
                    fm2_idx[:, 0] += batches * batch_size

                    # First sequence in the pair
                    fm1_vals = np.reshape(fm1_vals, [-1, 1])
                    fm1_position_info = np.hstack([fm1_idx, fm1_vals])
                    # Add binary value indicating which sequence in the pair (=0)
                    fm1_position_info = np.hstack([np.zeros([fm1_vals.shape[0], 1]), fm1_position_info])

                    # Second sequence in the pair
                    fm2_vals = np.reshape(fm2_vals, [-1, 1])
                    fm2_position_info = np.hstack([fm2_idx, fm2_vals])
                    # Add binary value indicating which sequence in the pair (=1)
                    fm2_position_info = np.hstack([np.ones([fm2_vals.shape[0],1]), fm2_position_info])

                    sample_position_info = np.vstack((fm1_position_info, fm2_position_info))

                    position_info.append(sample_position_info)

                    activities.append(activity)
                    predictions.append(pred)

        position_info = np.vstack(position_info)
        predictions = np.array(predictions)

        # Ouput values of each filter pair for each sample
        activities = np.array(activities)

        # Merge predictions
        predictions = np.vstack(predictions)[:len(labels)]
        activities = np.vstack(activities)[:len(labels)]

        # Evaluation metrics
        real = np.reshape(np.array(evaluation_label_list), [-1, 1])
        roc, accuracy, precision, specificity = self._evaluation_metrics(real, predictions)
        area = auc(roc[0], roc[1])

        return(roc, area, activities, predictions, accuracy, precision, specificity, position_info)


    def filters_to_seqlogo(self, save_dir):
        import sequencelogo
        filters = self.evaluated_filters
        #print("Ingoring negative values for WebLogo plotting!")
        #filters = np.maximum(filters, 0.000)

        for f_idx in range(filters.shape[-1]):
            filter = filters[:, :, f_idx]
            sequencelogo.plotseqlogo(filter, "%s/filter_%d" % (save_dir, f_idx))

