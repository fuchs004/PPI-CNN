import numpy as np
import pandas as pd

from bokeh.io import show, output_file, save
from bokeh.models.widgets import Tabs, Panel
from bokeh.resources import CDN
from bokeh.embed import file_html
from bokeh.layouts import gridplot
from bokeh import events
from bokeh.models import (
    ColumnDataSource,
    HoverTool,
    LinearColorMapper,
    BasicTicker,
    PrintfTickFormatter,
    ColorBar,
    TapTool,
    OpenURL,
    CustomJS

)
from bokeh.plotting import figure

def get_custom_hover_tool():
    hover = HoverTool(tooltips="""
        <div>
            <div>
            <span style="font-size: 16px;">Sample:</span>
            <span style="font-size: 17px; font-weight: bold;">@x</span>
                <img
                    src="@img1" height="50" alt="@imgs" width="200"
                    style="float: left; margin: 0px 15px 15px 0px;"
                    border="2"
                ></img>
            </div>
            <div>
                <img
                    src="@img2" height="50" alt="@imgs" width="200"
                    style="float: left; margin: 0px 15px 15px 0px;"
                    border="2"
                ></img>
            </div>
            <div>
                <span style="font-size: 15px;">Activation:</span>
                <span style="font-size: 17px; font-weight: bold;">@value</span>
            </div>
            <div>
                
                
            </div>
        </div>
        """
                      )
    #<span style="font-size: 10px; color: #696;">($x, $y)</span>
    return (hover)

def _create_prediction_heatmap(filter_activation_matrix, predictions, real_labels):
    predictions = ['+' if np.round(x) == 1 else '-' for x in predictions]
    # predictions = [predictions[x] + '\n+' if np.round(real_labels[x]) == 1 else predictions[x] + '\n-'for x in
    #               range(len(predictions))]
    n_filter_pairs = filter_activation_matrix.shape[0]
    filter_activation_matrix = np.transpose(filter_activation_matrix, [1, 0])

    colors = ["#75968f", "#a5bab7", "#c9d9d3", "#e2e2e2", "#dfccce", "#ddb7b1", "#cc7878", "#933b41", "#550b1d"]
    mapper = LinearColorMapper(palette=colors, low=np.min(filter_activation_matrix),
                               high=np.max(filter_activation_matrix))

    y_range = [str(n) for n in range(n_filter_pairs)]
    x_range = [str(n) for n in range(len(predictions))]

    x = ["%d" % n for n in range(len(predictions)) for i in range(n_filter_pairs)]
    y = y_range * len(predictions)

    values = filter_activation_matrix.ravel()
    img1 = ['filter_%d.png' % (f * 2) for _ in range(len(predictions)) for f in range(n_filter_pairs)]
    img2 = ['filter_%d.png' % (f * 2 + 1) for _ in range(len(predictions)) for f in range(n_filter_pairs)]
    source = ColumnDataSource({'x': x, 'y': y, 'value': values, 'img1': img1, 'img2': img2})

    hover = get_custom_hover_tool()
    TOOLS = [hover, "tap"]  # , "save,pan,box_zoom,reset,wheel_zoom"]

    p = figure(title="Prediction Motif Interpretation",
               x_range=x_range, y_range=list(reversed(y_range)),
               x_axis_location="above", plot_width=900, plot_height=400,
               tools=TOOLS, toolbar_location=None)

    p.xaxis.major_label_text_font_size = "5pt"
    # Axis settings
    p.yaxis.axis_label = "Filter pair"
    p.xaxis.axis_label = "Sample"
    p.axis.major_label_standoff = 0
    p.toolbar.logo = None

    p.rect(source=source, x='x', y='y', width=1, height=1, fill_color={'field': 'value', 'transform': mapper},
           name="rectangle")

    color_bar = ColorBar(color_mapper=mapper, major_label_text_font_size="5pt",
                         ticker=BasicTicker(desired_num_ticks=len(colors)),
                         formatter=PrintfTickFormatter(format="%.2f"),
                         label_standoff=6, border_line_color=None, location=(0, 0))
    p.add_layout(color_bar, 'right')

    return(source, p)

def _get_JS_callback():
    code = """
                var data = source.data,
                    selected = source.selected['1d']['indices'];

                var complete_data = complete_source.data;
                var sample_data_1 = sample_source_1.data;
                var sample_data_2 = sample_source_2.data;
                
                var selected_sample;
                
                if(selected.length == 1){
                    // only consider case where one glyph is selected by user
                    selected_filter = data['y'][selected[0]];
                    selected_sample = data['x'][selected[0]];
                    first_filter = selected_filter * 2;
                    second_filter = first_filter + 1;

                    var pos_arr = [[],[]];
                    var filter_arr = [[],[]];
                    var similarity_arr = [[],[]];
                    var seq_arr = [[],[]];
                    var sample_arr = [[],[]];
                    var index_arr = [[],[]];

                    for (var i = 0; i < complete_data['sample'].length; ++i){
                        if(complete_data['sample'][i] == selected_sample && ((complete_data['filter'][i] == first_filter) || 
                        (complete_data['filter'][i] == second_filter))){
                        
                            var seq = complete_data['filter'][i] - first_filter;
                            
                            pos_arr[seq].push(complete_data['position'][i]);
                            filter_arr[seq].push(complete_data['filter'][i]);
                            similarity_arr[seq].push(complete_data['similarity'][i]);
                            seq_arr[seq].push(complete_data['seq'][i]);
                            sample_arr[seq].push(complete_data['sample'][i]);
                            index_arr[seq].push(complete_data['index'][i]);
                        }
                    }

                }

                sample_data_1['position'] = pos_arr[0];
                sample_data_1['index'] = index_arr[0];
                sample_data_1['filter'] = filter_arr[0];
                sample_data_1['similarity'] = similarity_arr[0];
                sample_data_1['seq'] = seq_arr[0];
                sample_data_1['sample'] = sample_arr[0];
                
                sample_data_2['position'] = pos_arr[1];
                sample_data_2['index'] = index_arr[1];
                sample_data_2['filter'] = filter_arr[1];
                sample_data_2['similarity'] = similarity_arr[1];
                sample_data_2['seq'] = seq_arr[1];
                sample_data_2['sample'] = sample_arr[1];
                
                
                source.selected['1d']['indices'] = [];
                source.change.emit();
                complete_source.change.emit();
                sample_source_1.change.emit();
                sample_source_2.change.emit();
            """
    return code

def _create_motif_map(complete_df, source):
    colors = ["#75968f", "#a5bab7", "#c9d9d3", "#e2e2e2", "#dfccce", "#ddb7b1", "#cc7878", "#933b41", "#550b1d"]
    sample_df = complete_df[complete_df['sample'] == 41.0]
    sample_df_1 = sample_df[sample_df['filter'] == 14.0]
    sample_df_2 = sample_df[sample_df['filter'] == 15.0]

    # CDS for all samples
    complete_source = ColumnDataSource(complete_df)

    # CDS for the specific sample to be shown in the motif map
    sample_source_1 = ColumnDataSource(sample_df_1)
    sample_source_2 = ColumnDataSource(sample_df_2)

    ## Sample names
    #all_sample_names = pd.DataFrame({'name': sample_pair_list})

    # Javascript callback code to deal with rectangle selection in the heatmap
    code = _get_JS_callback()

    callback = CustomJS(args={'source': source, 'sample_source_1': sample_source_1, 'sample_source_2': sample_source_2,
                              'complete_source': complete_source},
                        code=code)
    source.callback = callback

    # Actual plotting
    pos_fig = figure(title="Motif similarity in sequence",
                     x_axis_location="below", plot_width=900, plot_height=400,
                     tools=['pan', 'zoom_in', 'zoom_out'], toolbar_location=None)

    col_mapper = LinearColorMapper(palette=colors, low=0.0, high=1.0)

    # Plot with glyphs where the y-axis indicates which of the two sequences in the sample (sequence pair)
    # The x-axis indicates the position in the sequence
    # The orientation of the triangle indicates which of the two filters in a filter pair
    # The colour of the triangles indicates the similarity score

    pos_fig.triangle(x="position", y="seq", legend='filter', fill_alpha='similarity', source=sample_source_1, size=20,
                 color={'field': 'similarity', 'transform': col_mapper})
    pos_fig.inverted_triangle(x="position", y="seq", legend='filter', fill_alpha='similarity', source=sample_source_2, size=20,
                 color={'field': 'similarity', 'transform': col_mapper})

    color_bar = ColorBar(color_mapper=col_mapper, major_label_text_font_size="5pt",
                         ticker=BasicTicker(desired_num_ticks=len(colors)),
                         formatter=PrintfTickFormatter(format="%.2f"),
                         label_standoff=6, border_line_color=None, location=(0, 0))
    pos_fig.add_layout(color_bar, 'right')

    pos_fig.legend.location = 'top_left'
    pos_fig.legend.click_policy = 'hide'

    # Axis settings
    pos_fig.yaxis.axis_label = "Sequence in pair"
    pos_fig.xaxis.axis_label = "Position"

    pos_fig.yaxis[0].ticker.desired_num_ticks = 2
    pos_fig.y_range.start = -0.5
    pos_fig.y_range.end = 1.5
    pos_fig.x_range.start = 0.0
    pos_fig.x_range.end = 500.0

    return pos_fig

def plot_filter_activation(filter_activation_matrix, predictions, save_dir, real_labels, position_activity):

    source, p = _create_prediction_heatmap(filter_activation_matrix, predictions, real_labels)

    complete_df = pd.DataFrame(position_activity, columns = ["seq", "sample", "position", "filter", "similarity"])

    pos_fig = _create_motif_map(complete_df, source)

    grid = gridplot([[pos_fig],[p]], toolbar_options={'logo': None})
    output_file("%s/interpretation.html"%save_dir, mode='inline')

    show(grid)  # show the plot


def sigmoid(x):
    return(1.0/(1.0+np.exp(-x)))


def create_interactive_visualization(model_dir, pair_activation_matrix=None, predictions=None, real_labels=None,
                                     only_positive=False, position_activity=None):
    pair_activation_matrix = np.load("%sevaluation_activity.npy" % model_dir)
    predictions = np.load("%sevaluation_predictions.npy" % model_dir)
    position_activity = np.load("%s/position_scores.npy" % model_dir)

    pair_weights = np.load("%sweight_2.npy" % model_dir)
    #if only_positive:
    #    pair_weights = np.exp(pair_weights)
    prediction_bias = np.exp(np.load("%sweight_3.npy" % model_dir))

    pair_activation_matrix = np.transpose(pair_activation_matrix)
    net_activity = np.multiply(pair_activation_matrix, pair_weights)

    relative_activity = net_activity / prediction_bias

    plot_filter_activation(relative_activity, predictions, model_dir, real_labels, position_activity=position_activity)

def main():
    create_interactive_visualization(
        model_dir="Results/Synthetic/Synthetic_14_length500_frac0.50"
                  "/HighLR_BoundPredWeights_SparsityTerm_24Filters_uniform_True_normalized/2/", real_labels="a",
        only_positive=True)

if __name__ == "__main__":
    main()