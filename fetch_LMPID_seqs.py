import urllib3
import os
from data_processing import create_tfrecord_sequence_only

def get_unique_ids(fname):
    with open(fname, 'r') as inp:
        ids = inp.read().replace('\n','\t')
        ids = list(set(ids.split('\t')[2:-1])) # skip headers and empty line
    return ids

def fetch_ids_from_uniprot(id_list, dir="Fasta/"):
    http = urllib3.PoolManager()

    seq_dict = {}
    for identifier in id_list:
        response = http.urlopen('GET', "http://www.uniprot.org/uniprot/%s.fasta" % (identifier))
        data = response.data
        seq = b''.join(data.split(b'\n')[1:]).decode()
        seq_dict[identifier] = seq
        print("Fetched", identifier)
        with open("%s%s" % (dir, identifier), 'w') as outp:
            outp.write(seq)
    return seq_dict

def fetch_ids_from_fasta_files():
    files = os.listdir("Fasta/")
    dict = {}
    for f in files:
        with open("Fasta/%s" %f,'r') as inp:
            fasta = inp.read()
            if ">" in fasta:
                continue
            dict[f] = fasta

    return dict

def main():
    ids = get_unique_ids("LMPID_pairs.tsv")
    seq_dict = fetch_ids_from_uniprot(ids)

    if not os.path.isdir("Biological Data"):
        os.mkdir("Biological Data")

    if not os.path.isdir("Biological Data/LMPID"):
        os.mkdir("Biological Data/LMPID")

    if not os.path.isdir("Biological Data/LMPID/Records"):
        os.mkdir("Biological Data/LMPID/Records")

    create_tfrecord_sequence_only(seq_dict, "Records")

if __name__ == "__main__":
    main()