import os
from random import shuffle
from data_processing import create_tfrecord_sequence_only

def get_sequences_from_file(filename, label):
    with open(filename, 'r') as inp:
        raw_text = inp.read()
        splitter = raw_text.split('>')
        line_splitter = raw_text.split('\n')

        pair_list = [x.split()[-2:] + ['%d'%label] for i, x in enumerate(line_splitter) if i%5==0][:-1]
        sequence_dict = dict([(x.split('\n')[0], x.split('\n')[1]) for x in splitter[1:]])

    if not os.path.isdir("Biological Data"):
        os.mkdir("Biological Data")

    if not os.path.isdir("Results"):
        os.mkdir("Results")

    if not os.path.isdir("Results/Biological"):
        os.mkdir("Results/Biological")

    if not os.path.isdir("Biological Data/Fasta"):
        os.mkdir("Biological Data/Fasta")

    if not os.path.isdir("Biological Data/Records"):
        os.mkdir("Biological Data/Records")

    for key in sequence_dict:
        with open("Biological Data/Fasta/%s"%key, 'w') as outp:
            outp.write(sequence_dict[key])

    with open("Biological Data/pairs_%s" %filename[-10:], 'w') as outp:
        pair_text = '\n'.join(['\t'.join(x) for x in pair_list])
        outp.write(pair_text)

    create_tfrecord_sequence_only(sequence_dict, "Biological Data/Records")

    return pair_list

def create_training_and_test_pairs(all_pairs, test_set_size=100):
    shuffle(all_pairs)

    training_set = all_pairs[test_set_size:]
    test_set = all_pairs[:test_set_size]

    with open("Biological Data/training_pairs.txt", 'w') as outp:
        pair_text = '\n'.join(['\t'.join(x) for x in training_set])
        outp.write(pair_text)

    with open("Biological Data/test_pairs.txt", 'w') as outp:
        pair_text = '\n'.join(['\t'.join(x) for x in test_set])
        outp.write(pair_text)

def main():

    # Negative samples
    print("Indexing negative samples...")
    neg_pairs = get_sequences_from_file("Biological Data/Supp-D.txt", label=0)
    # Positive samples

    print("Indexing positive samples...")
    pos_pairs = get_sequences_from_file("Biological Data/Supp-C.txt", label=1)

    all_pairs = neg_pairs + pos_pairs

    print("Creating training set and test set...")
    create_training_and_test_pairs(all_pairs)

    print("All done.")

if __name__ == "__main__":
    main()