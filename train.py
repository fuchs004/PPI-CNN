from CNN_PPI import PPI_CNN
from interpretation import create_interactive_visualization
from data_processing import *

def create_evaluation_files(save_dir, evaluation_pairs, evaluation_labels, evaluation_results,
                            only_positive_weights=True):
    roc, area, activation, predictions, accuracy, precision, specificity, position_scores = evaluation_results

    print("Accuracy:\t", accuracy)
    print("Precision:\t", precision)
    print("Specificity:\t", specificity)
    print("AUC-ROC:\t", area)
    with open("%s/Evaluation_metric.txt" % save_dir, 'w') as outp:
        outp.write("Accuracy:\t%.2f\n" % accuracy)
        outp.write("Precision:\t%.2f\n" % precision)
        outp.write("Specificity:\t%.2f\n" % specificity)
        outp.write("AUC-ROC:\t%.2f" % area)

    plot_roc(roc, area, "%s/roc.png" % save_dir)

    plot_evaluation_activity(save_dir, activation, predictions, evaluation_labels, only_positive=only_positive_weights)
    # Note that filters.npy is different from weight_0.npy
    # weight_0.npy are the weights before normalization
    filters = np.load("%s/filters.npy" % save_dir)
    bias = np.load("%s/weight_1.npy" % save_dir)
    #position_scores = get_motif_positions_in_sequence(evaluation_pairs, filters, bias)
    np.save("%s/position_scores" % save_dir, position_scores)

    create_interactive_visualization(save_dir, activation, predictions, evaluation_labels,
                                     position_activity=position_scores, only_positive=only_positive_weights)



def start_training(data_dir, base_save_dir, pair_file, eval_pair_file, attention, silence_edges, lr, fsr, n_filters, \
                                    batch_size,
                   init, act, kern_size, iterations, restore_model):
    evaluation_pairs, evaluation_labels = get_evaluation_pairs_from_file(data_dir, eval_pair_file)

    if init != 'uniform':
        print("Disabling false start, since uniform initialization was not chosen.")
        fsr = 0.0

    if not os.path.exists(base_save_dir):
        os.mkdir(base_save_dir)

    # Train the network in triplo
    for i in range(3):
        save_dir = base_save_dir + '/%d/' %i
        if not os.path.exists(save_dir):
            os.mkdir(save_dir)
        nn = PPI_CNN(data_dir=data_dir, batch_size=batch_size, seq_length="variable", kern_size=kern_size,
                     n_fil=n_filters, pair_file=pair_file, false_start_rate=fsr, init=init, io_threads=3, bound=1.0)

        nn.train(iterations, initial_lr=lr, dropout=0.0, save_filters=save_dir, save_to=save_dir,
                 adaptive_dropout=True, filter_activation=act, training_threads=3, random_seed=None,
                 restore_model=restore_model, attention_mask=attention, loss_func="cross-entropy",
                 silence_edges=silence_edges, early_stop=0.01)

        nn.filters_to_seqlogo(save_dir)

        # It is highly recommended to use batch_size=1 here to prevent memory issues
        # The reason for this is that we are, among others, requesting the entire feature map for all sequences in the
        # batch and constructing a matrix of of all non-zero indices.
        evaluation_results = nn.evaluate(evaluation_pairs, evaluation_labels, batch_size=1)
        del (nn)

        # Create ROC plot, interactive visualization for the filters and .txt files for the evaluation metrics
        create_evaluation_files(save_dir, evaluation_pairs, evaluation_labels, evaluation_results)


def main():
    # Settings
    attention = True # Attention is currently replaced with a sparsity term
    silence_edges = True
    lr = 3e-3   # Learning rate
    fsr_iter = 1.0 # The number of iterations before the next filter becomes trainable (in false start)
    n_filters = 74
    batch_size = 25
    init = 'uniform' # select ['uniform', 'seminormal']
    act = "normalized" # select ['normalized', 'softmax']
    kern_size = 11
    iterations = 60000
    fsr = fsr_iter * n_filters / float(iterations) # False start rate

    # Do we use biological or synthetic data to train?
    biological = False

    if biological:
        print("Using biological data.")
        # Biological Directories
        data_dir = "Biological Data/Records"
        pair_file = "Biological Data/training_pairs.txt"
        eval_pair_file = "Biological Data/test_pairs.txt"
        save_dir = "Results/Biological/%dFilters_%s_%s_%s/" % (n_filters,init,str(attention),act)
    else:
        # Synthetic Directories
        if not os.path.exists("Results/Synthetic/"):
            os.mkdir("Results/Synthetic/")

        # Change synth name to the name of the data you've generated, if necessary.
        # If you ran create_synthetic_data.py without making any changes, you can leave synth_name as is.
        synth_name = "Synthetic_14_length500_frac0.50"
        data_dir = "Synthetic Data/%s/Records" %synth_name
        pair_file = "Synthetic Data/%s/training_pairs.txt" %synth_name
        eval_pair_file = "Synthetic Data/%s/test_pairs.txt" %synth_name

        if not os.path.exists("Results/Synthetic/%s/"%synth_name):
            os.mkdir("Results/Synthetic/%s/"%synth_name)

        save_dir = "Results/Synthetic/%s/%dFilters_%s_%s_%s" \
                   "/" % (synth_name, n_filters, init, str(attention), act)


    # Do we want to load a previously trained model?
    #restore_model = save_dir + '/0/' # otherwise use "Results/insert_model_dir_here/"

    restore_model = None
    start_training(data_dir, save_dir, pair_file, eval_pair_file, attention, silence_edges, lr, fsr, n_filters, \
                    batch_size, init, act, kern_size, iterations, restore_model)

if __name__ == "__main__":
    main()