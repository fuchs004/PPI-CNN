
import matplotlib.pyplot as plt
from CNN_PPI import ppi_cnn
import seaborn as sns
import numpy as np
import os

def get_evaluation_activity(model_dir, evaluation_pair_file, data_dir, batch_size=50):
    with open(evaluation_pair_file, 'r') as inp:
        lines = inp.read().split('\n')[:-1]
        print(len(lines))
        pairs_1 = ["%s/%s" % (data_dir, x.split('\t')[0]) for x in lines]
        pairs_2 = ["%s/%s" % (data_dir, x.split('\t')[1]) for x in lines]
        evaluation_pairs = [pairs_1, pairs_2]

        evaluation_labels = [float(x.split('\t')[2]) for x in lines]


    network = ppi_cnn(data_dir, batch_size, "variable", 11, 0, "", false_start_rate=0.0,
                      init="uniform", io_threads=18, restore_model=model_dir)

    roc, area, activity, predictions, accuracy, precision, specificity = network.evaluate(evaluation_pairs,
                                          evaluation_labels, apply_sigmoid=True, batch_size=batch_size)

    print("Accuracy:\t", accuracy)
    print("Precision:\t", precision)
    print("Specificity:\t", specificity)
    with open("%s/Evaluation_metric.txt" %(model_dir), 'w') as outp:
        outp.write("Accuracy:\t%.2f\n" %accuracy)
        outp.write("Precision:\t%.2f\n" % precision)
        outp.write("Specificity:\t%.2f\n" % specificity)
        outp.write("AUC:\t%.2f" % area)

    print("Area:", area)
    del (network)

    return (activity, predictions, evaluation_labels)


def plot_filter_activation(filter_activation_matrix, predictions, save_dir, real_labels):

    predictions = ['+' if np.round(x) == 1 else '-' for x in predictions]
    predictions = [predictions[x] + '\n+' if np.round(real_labels[x]) == 1 else predictions[x] + '\n-'for x in
                   range(len(predictions))]

    ax = sns.heatmap(filter_activation_matrix, xticklabels=predictions)
    ax.set_title("Motif pair activity during evaluation")
    ax.set_ylabel("Filter pair")
    ax.set_xlabel("Samples")
    [t.set_color('green') if predictions[idx].split('\n')[0] == predictions[idx].split('\n')[1] else t.set_color(
        'red') for idx,t  in enumerate(ax.xaxis.get_ticklabels())]

    plt.gcf().text(0.01, 0.045, "Predicted\nReal", fontsize=11)
    plt.savefig("%sprediction_heatmap.png"%save_dir)
    plt.cla()
    plt.close()

def main():

    source_dir = "../Biological Results/Sun/SuppCD/Both/Uniform/"
    record_dir = "../Data/Sun/Records"

    for model_dir in os.listdir(source_dir):
        #model_dir = "OnlyInteracting_Bound3_22Filters_uniform_20ScaledAttentionTrue_normalized_Sigmoid_2"
        model_dir = source_dir + model_dir + '/'
        pair_file = "%s/sorted_Supp-CD_evaluation_pairs.txt" % (record_dir)
        #pair_file = "%s/sorted_biological_evaluation_pairs.txt" %record_dir

        # This should be the same batch size as was used to train the network
        batch_size = 25

        # First time: run the model to evaluate the specified pairs and record the activity of the filter pair nodes.
        pair_activation_matrix, predictions, real_labels = get_evaluation_activity(model_dir, pair_file, record_dir,
                                                                                   batch_size=batch_size)
        np.save("%sevaluation_activity.npy" %model_dir, pair_activation_matrix)
        np.save("%sevaluation_predictions.npy" % model_dir, predictions)
        pair_activation_matrix = np.load("%sevaluation_activity.npy" %model_dir)

        predictions =  np.load("%sevaluation_predictions.npy" %model_dir)

        pair_weights = np.load("%sweight_2.npy"%model_dir)
        #only pos:
        pair_weights = np.exp(pair_weights)
        prediction_bias = np.exp(np.load("%sweight_3.npy"%model_dir))
        #post_prediction_bias = np.load("%sweight_4.npy"%model_dir)

        pair_activation_matrix = np.transpose(pair_activation_matrix)

        net_activity = np.multiply(pair_activation_matrix, pair_weights)

        relative_activity = net_activity / prediction_bias

        plot_filter_activation(relative_activity, predictions, model_dir, real_labels)

if __name__ == "__main__":
    main()