import numpy as np
import os
import tensorflow as tf
import matplotlib.pyplot as plt
import seaborn as sns

allowed_characters = ['H', 'R', 'K', 'I', 'P', 'F', 'Y', 'Q', 'E', 'D',
                      'C', 'W', 'T', 'L', 'N', 'G', 'A', 'M', 'V', 'S'] #, 'U']

len_allowed_char = len(allowed_characters)

def convolve_array(array, kernel):
    """
    Convolve/cross-correlate array with kernel

    :param array:
    :param kernel:
    :return:
    """
    # kernel width
    k_w = kernel.shape[0]
    values = []
    for i in range(array.shape[0] - k_w + 1):
        arr_slice = array[i:i+k_w]
        val = np.sum(arr_slice * kernel)
        values.append(val)
    max_val = np.max(values)
    return(max_val)

def correlate_filters_and_motifs(filter_array, motif_array):
    """
    Calculates the maximum cross-correlation between every filter in filter_array and every motif in motif_array.
    :param filter_array: numpy array, of the format [filter_length, n_features, number_of_filters]
    :param motif_array: numpy array, of the format [motif_length, n_features, number_of_motifs]
    :return: list of lists, of the format [[filter_0_motif_0, filter_0_motif_1, ..., filter_0_motif_k],
    ... [filter_N_motif_0,  filter_N_motif_1, ..., filter_N_motif_k]]
    """
    filter_max_vals = []

    # For every filter
    for f_idx in range(filter_array.shape[-1]):
        fil = filter_array[:, :, f_idx]
        fil = np.pad(fil, ((motif_array.shape[0], motif_array.shape[0]), (0, 0)), mode='constant')
        correlation_scores = []

        # Calculate the cross correlation for every motif
        for m_idx in range(motif_array.shape[-1]):
            motif = motif_array[:, :, m_idx]
            max_val = convolve_array(fil, motif)
            correlation_scores.append(max_val)
        filter_max_vals.append(correlation_scores)
    return(filter_max_vals)

def mean_cross_correlation(filter_array, motif_array):
    """
    Calculates the mean cross-correlation between filters and motifs. Indicates how well the true motifs are learnt
    by the network.
    :param filter_array: numpy array, of the format [filter_length, n_features, number_of_filters]
    :param motif_array: numpy array, of the format [motif_length, n_features, number_of_motifs]
    :return: Float, representing the mean paired cross-correlation
    """
    # For each filter, get the maximum cross-correlation for all motifs
    filter_max_vals = correlate_filters_and_motifs(filter_array, motif_array)
    filter_max_vals = np.array(filter_max_vals)
    motif_max_vals = np.max(filter_max_vals, axis=0)

    return(np.mean(motif_max_vals))

def mean_paired_cross_correlation(filter_array, motif_array):
    """
    Calculates the mean cross-correlation between filter pairs and motif pairs. This gives an indication of how well
    the network represents the complementary motifs in complementary filter pairs.
    :param filter_array: numpy array, of the format [filter_length, n_features, number_of_filters]
    :param motif_array: numpy array, of the format [motif_length, n_features, number_of_motifs]
    :return: Float, representing the mean paired cross-correlation
    """

    # For each filter, get the maximum cross-correlation for all motifs
    filter_max_vals = correlate_filters_and_motifs(filter_array, motif_array)

    # Add the scores of complementary motifs in complementary filters
    filter_pair_scores = []
    for f_idx in range(int(len(filter_max_vals)/2)):
        pair = filter_max_vals[f_idx*2:f_idx*2+2]
        pair_scores = []
        for i in range(int(len(pair[0])/2)):
            # Keep only the maximum of the two possible orientations (A,B), (B,A)
            paired_val = max(pair[0][i*2] + pair[1][i*2+1], pair[1][i*2] + pair[0][i*2+1])
            pair_scores.append(paired_val)
        filter_pair_scores.append(pair_scores)

    # At this point filter_pair_scores contains, for each filter pair, its highest cross-correlation with all motif
    # pairs, so now we only keep the maximum value in each column (i.e. for each filter pair). This also gets rid of
    # any bias in the score due to motifs modeled twice

    filter_pair_scores = np.max(np.array(filter_pair_scores), axis=0)

    # Next we divide this score by 2 to get the mean (because earlier we sum two cross-correlations together)
    filter_pair_scores /= 2.0

    # Now we have a score of how well each pair is modeled. We take the mean to get an overall score
    mean_score = np.mean(filter_pair_scores)

    return(mean_score)

def plot_roc(roc, area, save_file):
    plt.plot(roc[0], roc[1], label="AUC: %.2f"%area)
    diag = [i/100.0 for i in range(101)]
    plt.plot(diag,diag, '--')
    plt.ylabel("True positive rate")
    plt.xlabel("False positive rate")
    plt.legend(loc="lower right")
    plt.title("Receiver Operator Characteristic")
    plt.savefig(save_file)
    plt.cla()
    plt.close()

def get_evaluation_pairs_from_file(data_dir, evaluation_pair_file):
    with open(evaluation_pair_file, 'r') as inp:
        lines = inp.read().split('\n')[:-1]
        pairs_1 = ["%s/%s"%(data_dir, x.split('\t')[0]) for x in lines]
        pairs_2 = ["%s/%s"%(data_dir, x.split('\t')[1]) for x in lines]

        evaluation_pairs = [pairs_1, pairs_2]
        evaluation_labels = [float(x.split('\t')[2]) for x in lines]

    return(evaluation_pairs, evaluation_labels)

def plot_filter_activation(filter_activation_matrix, predictions, save_dir, real_labels):
    predictions = ['+' if np.round(x) == 1 else '-' for x in predictions]
    predictions = [predictions[x] + '\n+' if np.round(real_labels[x]) == 1 else predictions[x] + '\n-'for x in
                   range(len(predictions))]

    ax = sns.heatmap(filter_activation_matrix, xticklabels=predictions)
    ax.set_title("Motif pair activity during evaluation")
    ax.set_ylabel("Filter pair")
    ax.set_xlabel("Samples")
    [t.set_color('green') if predictions[idx].split('\n')[0] == predictions[idx].split('\n')[1] else t.set_color(
        'red') for idx,t  in enumerate(ax.xaxis.get_ticklabels())]

    plt.gcf().text(0.01, 0.045, "Predicted\nReal", fontsize=11)
    plt.savefig("%sprediction_heatmap.png"%save_dir)
    plt.cla()
    plt.close()

def sigmoid(x):
    return (1.0/(1.0 + np.exp(-x)))

def plot_evaluation_activity(model_dir, pair_activation_matrix, predictions, real_labels, only_positive=False):
    np.save("%sevaluation_activity.npy" % model_dir, pair_activation_matrix)
    np.save("%sevaluation_predictions.npy" % model_dir, predictions)

    activation_matrix = np.load("%sevaluation_activity.npy" % model_dir)

    predictions = np.load("%sevaluation_predictions.npy" % model_dir)

    pair_weights = np.load("%sweight_2.npy" % model_dir)
    #if only_positive:
    #    pair_weights = np.exp(pair_weights)


    prediction_bias = np.exp(np.load("%sweight_3.npy" % model_dir))

    activation_matrix = np.transpose(activation_matrix)
    net_activity = np.multiply(activation_matrix, pair_weights)

    relative_activity = net_activity / prediction_bias

    plot_filter_activation(relative_activity, predictions, model_dir, real_labels)


def _parse_tfrecords(seq1, seq2, label):
    prediction_nodes = 1
    feature = {'train/seq': tf.FixedLenFeature([], tf.string),
               'train/seqlength': tf.FixedLenFeature([], tf.int64)}

    features = tf.parse_single_example(seq1, features=feature, name="parser1")
    chars = tf.constant(len(allowed_characters), dtype=tf.int32)
    seq1_length = tf.cast(features["train/seqlength"], tf.int32, name="seq1_length")
    seq1_length = tf.reshape(seq1_length, ())
    seq1 = tf.decode_raw(features["train/seq"], tf.int8, name="seq1_decode")
    size1 = tf.stack([seq1_length, chars])
    seq1 = tf.cast(tf.reshape(seq1, size1), tf.float32, name="seq1")

    feature = {'train/seq': tf.FixedLenFeature([], tf.string),
               'train/seqlength': tf.FixedLenFeature([], tf.int64)}
    features = tf.parse_single_example(seq2, features=feature, name="parser2")
    seq2_length = tf.cast(features["train/seqlength"], tf.int32, name="seq2_length")
    seq2_length = tf.reshape(seq2_length, ())
    seq2 = tf.decode_raw(features["train/seq"], tf.int8, name="seq2_decode")
    size2 = tf.stack([seq2_length, chars])
    seq2 = tf.cast(tf.reshape(seq2, size2), tf.float32, name="seq2")
    label = tf.reshape(tf.cast(label, dtype=tf.float64), [prediction_nodes])
    return (seq1, seq2, label)

def sequence_processing(batch_size, pair_list, labels, shuffle=True, threads=15, n_prediction_nodes=1):
    with tf.variable_scope("DataProcessing"):
        pl1 = tf.constant(pair_list[0])
        pl2 = tf.constant(pair_list[1])

        labels = np.array(labels)
        labs = tf.constant(labels, dtype=tf.float64)

        # How many prediction nodes are there?
        prediction_nodes = n_prediction_nodes

        # Create a dataset for the each sequence in a pair and the pairs label.
        # (A, B, label)
        ds1 = tf.data.TFRecordDataset(pl1)  # A
        ds2 = tf.data.TFRecordDataset(pl2) # B
        ds3 = tf.data.Dataset.from_tensor_slices(labs) # label

        combined_ds = tf.data.Dataset.zip((ds1, ds2, ds3))
        combined_ds = combined_ds.repeat() # Make the dataset repeat so we don't run out of sequences

        if shuffle: # Should be turned off when evaluating since we'll be comparing it to an ordered list of labels
            print("Shuffling samples. If you see this message during evaluation then something went wrong.")
            combined_ds = combined_ds.shuffle(batch_size)

        # Pecode the TFRecord formatted sequences
        combined_ds = combined_ds.map(_parse_tfrecords, num_parallel_calls=threads)

        # Apply padding where needed such that every sequence has the same size as the largest sequence
        pad_shape = ([-1, len(allowed_characters)], [-1, len(allowed_characters)], [prediction_nodes])
        combined_ds = combined_ds.padded_batch(batch_size, padded_shapes=pad_shape)

        iter = combined_ds.make_initializable_iterator()

        seq1, seq2, label = iter.get_next()
        seq1 = tf.reshape(seq1, [batch_size, -1, len(allowed_characters)])
        seq2 = tf.reshape(seq2, [batch_size, -1, len(allowed_characters)])

        label = tf.reshape(label, [batch_size, prediction_nodes])

        return (seq1, seq2, label, iter.initializer)

def minus_one_hot(character):
    if character.upper() == "X" or character.upper() == "U":
        vec = [-1 for _ in range(len(allowed_characters))]
    else:
        vec = [-1 for _ in range(len(allowed_characters))]
        idx = allowed_characters.index(character.upper())
        vec[idx] = 1
    return(vec)

def one_hot(character):
    if character.upper() == "X" or character.upper() == "U":
        vec = [0 for _ in range(len(allowed_characters))]
    else:
        vec = [0 for _ in range(len(allowed_characters))]
        idx = allowed_characters.index(character.upper())
        vec[idx] = 1
    return(vec)

def right_position(x, i, j):

    if x == allowed_characters.index(i) or x - len_allowed_char == allowed_characters.index(j):
        return 1
    else:
        return 0

def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def create_tfrecord_sequence_only(seq_dict, save_dir, minus=False, one_hot_conversion=True):
    if not os.path.isdir(save_dir):
        os.mkdir(save_dir)
    for name in seq_dict:
        if name == '':
            continue

        if one_hot_conversion:
            if minus:
                seq = np.array([minus_one_hot(AA) for AA in seq_dict[name]], dtype=np.int8)
            else:
                seq = np.array([one_hot(AA) for AA in seq_dict[name]], dtype=np.int8)
        else:
            seq = seq_dict[name]

        print("Creating TFRecord for entry", name)
        feature = {"train/seqlength": _int64_feature(seq.shape[0]),
                   "train/seq": _bytes_feature(seq.tostring())}

        to_save = tf.train.Example(features=tf.train.Features(
            feature=feature))
        writer = tf.python_io.TFRecordWriter("%s/%s" % (save_dir, name))
        writer.write(to_save.SerializeToString())

def get_allowed_characters():
    return(allowed_characters)
