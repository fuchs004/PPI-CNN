# Adapted from https://baoilleach.blogspot.nl/2017/05/using-weblogo3-to-create-sequence-logo.html
import numpy as np
try:
    from StringIO import StringIO
except ImportError:
    import io as StringIO

import weblogolib as w

class RefSeqColor(w.ColorRule):
    """
    Color the given reference sequence in its own color, so you can easily see
    which positions match that sequence and which don't.
    """

    def __init__(self, ref_seq, color, description=None):
        self.ref_seq = ref_seq
        self.color = w.Color.from_string(color)
        self.description = description

    def symbol_color(self, seq_index, symbol, rank):
        if symbol == self.ref_seq[seq_index]:
            return self.color

baserules = [
            w.SymbolColor("GSTYC", "green", "polar"),
            w.SymbolColor("NQ", "purple", "neutral"),
            w.SymbolColor("KRH", "blue", "basic"),
            w.SymbolColor("DE", "red", "acidic"),
            w.SymbolColor("PAWFLIMV", "black", "hydrophobic")
        ]

def plotseqlogo(filter, name):
    protein_alphabet = w.Alphabet("HRKIPFYQEDCWTLNGAMVS", [])
    kern_size = filter.shape[0]
    refseq = "A"*kern_size

    colorscheme = w.ColorScheme([RefSeqColor(refseq, "orange", "refseq")] + baserules,
                                alphabet = protein_alphabet)

    print("Sum:\t", np.sum(filter))
    if np.sum(filter) > 0.0:
        data = w.LogoData.from_counts(protein_alphabet, counts=filter)
        options = w.LogoOptions()
        # options.logo_title = name
        options.show_fineprint = False
        options.color_scheme = colorscheme
        options.yaxis_label = "Bit score"
        options.xaxis_label = "Filter position"
        options.resolution = 1000
        mformat = w.LogoFormat(data, options)

        fname = "%s.png" % name
        with open(fname, "wb") as f:
            try:
                f.write(w.png_formatter(data,mformat))
            except:
                pass
            #f.write(w.pdf_formatter(data, mformat))
    else:
        print("Empty filter '%s', skipping WebLogo generation for this filter."%name)

if __name__ == "__main__":
    pass